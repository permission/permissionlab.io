# Start Coding on the Permission ASK Blockchain
You can get started quickly coding dapps for the Permission ASK blockchain with Truffle/Ganache.


1. Download Ganache/Truffle [here](https://www.trufflesuite.com/ganache)
2. You will want to modify the configuration file for truffle in your local copy, adding in the network and chain id for the ASK Testnet:*snippet of code showing the values in context*
3. Start your copy of Ganache. *example starting Ganache*
4. Create an ASK compatible wallet.  The easiest way is by signing up at https://my.permission.io but we also have some [instructions on how to configure MetaMask to manage your ASK coins](link to instructions). 
5. Get some ASK Testnet tokens at the [ASK Faucet](https://permission.io/faucet)
6. Hello World Example
7. Let’s create a simple Solidity dapp and deploy it to the ASK Testnet.
8. Deploy your dapp to the ASK Testnet
9. Verify your dapp deployed
10. What’s Next?
