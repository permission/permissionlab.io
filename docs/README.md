---
home: true
navbar: false
sidebar: false
---

Want to take your place in the Permission.io ecosystem developing dApps for users that value their time and data?

<div class="two-columns">
  <div>
    <div class="centered-text">
      <h3><a href="/start-coding.html">Start Coding</a></h3>
      <a href="/faq/">FAQ</a>
    </div>
  </div>
  <div>
    <div class="centered-text">
      <h3><a href="/ASK-metamask-wallet.html">MetaMask Wallet with ASK</a></h3>
      <a href="/blockchain/mainnet/wallets.html">Other Mainnet Wallets</a>
    </div>
  </div>
</div>

<div class="two-columns">
  <div>
    <div>
      <a href="https://explorer-testnet.permission.io" target="_blank">ASK Testnet Explorer</a></div>
    <div>
      <a href="https://dev.permission.io/faucet/" target="_blank">ASK Testnet Faucet</a></div>
    <div>
      <a href="https://gitlab.com/permission/permission-node" target="_blank">Run a Testnet Node</a>
    </div>
  </div>
  <div>
    <div><a href="https://explorer.permission.io" target="_blank">ASK Mainnet Explorer</a></div>
    <div><a href="https://gitlab.com/permission/permission-node" target="_blank">Run a Mainnet Node</a></div>
  </div>
</div>

<div class="info-box-container">
  <div class="info-box">
    <div><a href="/blockchain/testnet.html">Permission Testnet in Detail</a></div>
    <div><span>Testnet Network ID</span><span>2221</span></div>
    <div><span>Testnet Chain ID</span><span>220</span></div>
    <div><span>Wallet derivation path (BIP44)</span><span>m/44'/2221'/0'</span></div>
    <div><span>Minimum required gas price</span><span>4,761,904,761 gASKwei</span></div>
    <div><span>Minimum required gas limit</span><span>21,000 gas</span></div>
    <div><span>Testnet Blockchain API node</span><span>https://blockchain-api-testnet.permission.io/rpc
wss://blockchain-api-testnet.permission.io/ws</span></div>
  </div>
  <div class="info-box">
    <div><a href="/blockchain">Permission Mainnet in Detail</a></div>
    <div><span>Mainnet Network ID</span><span>2221</span></div>
    <div><span>Mainnet Chain ID</span><span>222</span></div>
    <div><span>Wallet derivation path (BIP44)</span><span>m/44'/2221'/0'</span></div>
    <div><span>Minimum required gas price</span><span>4,761,904,761 gASKwei</span></div>
    <div><span>Minimum required gas limit</span><span>21,000 gas</span></div>
    <div><span>Mainnet Blockchain API node</span><span>https://blockchain-api-mainnet.permission.io/rpc
wss://blockchain-api-mainnet.permission.io/ws</span></div>
  </div>
  </div>
<br/><br/>
</div>