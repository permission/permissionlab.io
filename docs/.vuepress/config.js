const path = require('path');

module.exports = {
	title: 'Get Started Building with Permission',
	description: 'Reach new users who are seeking data ownership and compensation',
	/*
	locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    '/': {
      lang: 'en-US', // this will be set as the lang attribute on <html>
      title: 'Permission blockchain',
			description: 'Everything you want to know about the Permission blockchain',
    },
    '/zh/': {
      lang: 'zh-CN',
      title: 'Permission blockchain zh',
			description: 'Everything you want to know about the Permission blockchain zh',
    }
  },*/
	markdown: {
		lineNumbers: true
	},
	evergreen: true,
	dest: 'public',
	configureWebpack: {
		resolve: {
			alias: {
				'@images': 'images'
			}
		}
	},
	head: [
		['link', { rel: 'icon', href: '/favicon.ico' }],
		['script', {}, `
        var _hmt = _hmt || []; 
        (function() { 
        var globalNav = document.createElement('script');
				globalNav.src = 'https://shopping.permission.io/global-nav-app-dist/main.js';
				var globalFooter = document.createElement('script'); 
        globalFooter.src = 'https://shopping.permission.io/global-footer-app-dist/main.js';
        var s = document.getElementsByTagName('script')[0]; 
				s.parentNode.insertBefore(globalNav, s);
				s.parentNode.insertBefore(globalFooter, s);
        })(); 
    `]
	],
	themeConfig: {
		locales: {
      '/': {
        // text for the language dropdown
        selectText: 'Languages',
        // label for this locale in the language dropdown
        label: 'English',
        // Aria Label for locale in the dropdown
        ariaLabel: 'Languages',
        // text for the edit-on-github link
        editLinkText: 'Edit this page on GitHub',
        // config for Service Worker
        serviceWorker: {
          updatePopup: {
            message: "New content is available.",
            buttonText: "Refresh"
          }
				},
				navbar: false,
        // algolia docsearch options for current locale
        algolia: {},
				sidebar: false, /*{
					'/blockchain/': [
						{
							title: 'Mainnet',
							collapsable: false,
							children: [
								['', 'Introduction'],
								['mainnet/api', 'API Access'],
								['mainnet/wallets', 'Wallets'],
								['mainnet/geth', 'Running Geth'],
							]
						}, {
							title: 'Testnet',
							collapsable: false,
							children: [
								['testnet', 'Introduction'],
								['testnet/api', 'API Access'],
								['testnet/wallets', 'Wallets'],
								['testnet/geth', 'Running Geth'],
							]
						}
					]
				}*/
      },
      '/zh/': {
        selectText: '选择语言',
        label: '简体中文',
        editLinkText: '在 GitHub 上编辑此页',
        serviceWorker: {
          updatePopup: {
            message: "发现新内容可用.",
            buttonText: "刷新"
          }
        },
        algolia: {},
        nav: [
					{ text: 'Home zh', link: '/' },
					{ text: 'Blockchain zh', link: '/blockchain/' },
					{ text: 'FAQ zh', link: '/faq/' },
				],
				sidebar: {
					'/blockchain/': [
						{
							title: 'Mainnet zh',
							collapsable: false,
							children: [
								['', 'Introduction zh'],
								['mainnet/api', 'API Access zh'],
								['mainnet/wallets', 'Wallets zh'],
								['mainnet/geth', 'Running Geth zh'],
							]
						}, {
							title: 'Testnet zh',
							collapsable: false,
							children: [
								['testnet', 'Introduction zh'],
								['testnet/api', 'API Access zh'],
								['testnet/wallets', 'Wallets zh'],
								['testnet/geth', 'Running Geth zh'],
							]
						}
					]
				}
      }
		}
	}
};