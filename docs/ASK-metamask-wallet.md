# MetaMask Wallet with ASK
MetaMask is a free and open-source browser-based crypto wallet that is compatible with ASK, with some simple configurations.

<table>
  <tr>
    <td>
      <ol>
        <li class="step-one">Install MetaMask if you haven’t already.  You can get it <a href="https://metamask.io/" target="_blank">here</a> (https://metamask.io/)
        <br/><br/>
        If you're interested in the MetaMask source, their github repo for the browser extension is <a href="https://github.com/MetaMask/metamask-extension" target="_blank">here</a> (https://github.com/MetaMask/metamask-extension)</li>
        <li class="step-two">New MetaMask users will need to import or create an ETH wallet by default.</li>
        <li class="step-three">Select the top-right drop-down to expose a list of blockchain networks and click on “Custom RPC at the bottom”</li>
        <li>You should see a screen allowing you to enter blockchain settings to connect to and create an ASK or ASK Testnet wallet</li>
      </ol>
    </td>
    <td>
      <img src="images/metamask-step-1.png" alt="Select top drop-down for a list of blockchain networks" class="step-one">
      <img src="images/metamask-step-2.png" alt="" class="step-two">
      <img src="images/metamask-step-3.png" alt="click on 'Custom RPC at the bottom'" class="step-three">
    </td>
  </tr>
</table>

**You will want to enter the following (to set up a Testnet MetaMask ASK Wallet)**
| | |
| ----------- | ----------- |
| Network Name | ASK Testnet |
| New RPC URL | https://blockchain-api-testnet.permission.io/rpc |
| ChainID | 220 |
| Block Explorer URL | https://explorer-testnet.permission.io/ |

<br />

**You will want to enter the following (to set up a MetaMask ASK Wallet)**
| | |
| ----------- | ----------- |
| Network Name | ASK Mainnet |
| New RPC URL | https://blockchain-api-mainnet.permission.io/rpc |
| ChainID | 222 |
| Block Explorer URL | https://explorer.permission.io/ |

5. Hit Save at the bottom of the screen.
6. Head on over to the [Faucet to get your first Testnet ASK](https://faucet.permission.io)


