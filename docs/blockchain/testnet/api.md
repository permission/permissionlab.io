## Public API nodes

Permission.io hosts a public testnet API node that exposes both JSONRPC and websocket protocols.

Both API types can be accessed via these urls:

```text
https://blockchain-api-testnet.permission.io/rpc
wss://blockchain-api-testnet.permission.io/ws
```

## Connecting with web3.js

::: tip
The Permission blockchain uses a Proof of Authority based consensus algorithm and leverages the go-ethereum codebase, most Ethereum tools should work with a little bit of configuration.
:::

```bash
yarn add web3
```

```typescript
const Web3 = require('web3');
import { Block } from 'web3/eth/types'

const httpProvider = new Web3.providers.HttpProvider('https://blockchain-api-testnet.permission.io/rpc', 2500);
const client = new Web3(httpProvider);

client.eth.getBlock('latest')
.then((block: Block) => {
	console.log(block);
});
```