# ASK TestNet

## TestNet Blockchain information

| | |
| --- | --- |
| Network ID | 2221 |
| Chain ID | 220 |
| Block time | 20 seconds |
| Consensus | Proof of Authority (Clique) |
| Wallet derivation path | ```m/44'/2222'/0'/0``` |
| Minimum required gas price | ```4,761,904,761 gASKwei``` |
| Minimum required gas limit | ```21,000 gas``` |

## Bootnodes

The list of active bootnodes can be found here:

[https://s3.amazonaws.com/ask-blockchain/testnet/bootnodes.json](https://s3.amazonaws.com/ask-blockchain/testnet/bootnodes.json)

```json
[
  "enode://3f3a78fff527e3fad816cffcab0cb324040d5c9861929eb76b854a29bd9b84de91d00a609765850340bced5fadc785619133aef89cd6936cb8ead3e2fb2a1872@3.220.70.78:20202",
  "enode://075ffa8d7f85021097e088df7623c2f2d5a53758d40a3e77acf9d8f74d4c8dd341a66fb81b3a7310c48a7870a9389cb05565962c429593bd44e7875e9e002313@3.14.24.251:20202",
  "enode://535563e99855ec5f2dba87c9365720aecb6bba193699160f776008a28b30ce3c99e9d978da6ef5f159cad55ade6c8db1fbc1f17d35ef36add362a37095f28d0d@54.68.121.166:20202"
]
```

## Authority Nodes

The list of active authority nodes can be found here:

[https://s3.amazonaws.com/ask-blockchain/testnet/static-nodes.json](https://s3.amazonaws.com/ask-blockchain/testnet/static-nodes.json)

```json
[
  "enode://325c2bb361903526253453207801a96ed026f9eeb29635d5236be4875b98fb754a942f6673e874561f4331e82fe630da87f36f85e6779d24cfb4839626cf8dc6@3.220.70.78:20203",
  "enode://8621a3d05cdcf7800ab7a2935f04e526c26caab0aa8322dd88bbded5bc12e478adea0547b3aeb9af54971649b346e2fe8bb1a21b3716c9b19ccb2ad685d6171d@3.220.70.78:20204",
  "enode://b31a5dd4536ecaa823fb79af7c4428f893e50cca841d0d7f9661357a5004c992fa46f20afc93a3ab492dc6ff1b1c33a9fedd9bab5cdec7944d3ea42222451689@3.14.24.251:20203",
  "enode://24a697600a634d6260b8d026e523633c7ad523c3404dfba04896698e54f8a1dd7ed9cf75ab3969006810d3ad4427919f9810689ae7559e3c16dde6eef89be9ab@3.14.24.251:20204",
  "enode://2a86a261f1fcb331896526d6b12f9e91901b3bb92ad1a6072c7aff0b39b204def3de12d05ea60418b11fd8cc87665c709996a347fc042aa1b51c95e21336ce20@54.68.121.166:20203",
  "enode://749e906944fbb02c7dabd4db76592c8824100860c52d613820a117d283eb72de23959d27a2b9feac57013854994cc68e0882357e7e6ab21562308de0c215203e@54.68.121.166:20204"
]
```

## Genesis block

The genesis block can be found here:

[https://s3.amazonaws.com/ask-blockchain/testnet/genesis.json](https://s3.amazonaws.com/ask-blockchain/testnet/genesis.json)

```json
{
  "config": {
    "chainId": 220,
    "homesteadBlock": 0,
    "eip150Block": 0,
    "eip155Block": 0,
    "eip158Block": 0,
    "eip160Block": 0,
    "byzantiumBlock": 0,
    "clique": {
      "period": 20,
      "epoch": 30000
    },
    "eip150Hash": "0xdc655cda55fd6dd525109c51d02603db5541bf208b64526bd30a9d77ac48a524"
  },
  "parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "nonce": "0x0",
  "timestamp": "0x5d13a29a",
  "extraData": "0x000000000000000000000000000000000000000000000000000000000000000008cb090a7ad6ae0d6b8449c980178776b0c2636ae635db8e015787d1132ebf89cab8a3d8a9eae0ef3b5f3c58c2021805ba13f732caa68970e6ab411243306c2766dfe120ad2675e3f173335033b2e547d93e015133a13683c015b02e8c936d02c59b7b96069f56d28a3acbc838ad44649c521bdaa30cbdaf0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
  "gasLimit": "4000000000",
  "difficulty": "0x1",
  "mixhash": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "coinbase": "0x0000000000000000000000000000000000000000",
  "alloc": {
    "0000000000000000000000000000000000000000": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000001": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000002": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000003": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000004": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000005": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000006": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000007": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000008": {
      "balance": "0x1"
    },
    "951fed2fa4d24f73b75028ad3d076fef8232621c": {
      "balance": "100000000000000000000000000000"
    }
  }
}
```
