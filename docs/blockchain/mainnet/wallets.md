---
sidebarDepth: 2
---

# Wallets

## Permission.io

Permission.io offers a basic, hosted wallet secured by [Hashicorp Vault](https://www.vaultproject.io/) as part of [my.permission.io](https://my.permission.io).

![permission.io wallet](/images/permission-wallet.png)

## Third party/Open source

### MyCrypto

Since the mainnet blockchain is so new, a custom build of MyCrypto with support for the ASK token can be found at [mycrypto.com](https://mycrypto.com).

Use the network switcher in the top right corner, click "Show other networks", and select "Permission".

![MyCrypto ASK network](/images/mycrypto-testnet.png)

### Trezor

#### Setup and initialization

To set up your new Trezor wallet, follow the guide over at [trezor.io/start](https://trezor.io/start). Once you've set up your wallet, come back to this guide.

#### Using with the Permission mainnet blockchain

Once you have your Trezor setup, navigate your browser to [https://mycrypto.com](https://mycrypto.com) and select the "Permission (mainnet)" network from the dropdown located at the top right (follow the directions above).

Once you've selected the Permission (mainnet), click the Trezor wallet icon in the middle of the page and follow the prompts.

Once MyCrypto connects to your Trezor, it'll ask to import your key:

![MyCrypto ASK network](/images/trezor-export-public-key.png)

It registers as an unknown coin because while the Permission blockchain leverages the underlying Ethereum technology via [Geth](https://github.com/ethereum/go-ethereum), our network IDs and chain IDs are different.

Fear not! Things will still work. After clicking "export" you should be presented with a screen to select the wallet you would like to use:

![MyCrypto ASK network](/images/trezor-select-address-testnet.png)

Once unlocked, you'll be able to interact with the wallet you selected.


### ImToken

Coming soon!

### Trust Wallet

Coming soon!

### Edge

Coming soon!

## Manual wallet creation

Coming soon!
