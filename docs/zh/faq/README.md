# Frequently Asked Questions

## Is ASK an ERC20 token?

No. The Permission blockchain exists entirely outside of Ethereum.

## Can I send ASK tokens to my Ethereum wallet address?

Technically yes, but you probably shouldnt. Although the Permission blockchain leverages the go-ethereum software, it is a completely different chain than Ethereum.

If you send ASK tokens to an Ethereum address, _**it is possible they are gone forever**_! Whether they are gone or not depends entirely on how the account was generated.

Permission does not support sending to Ethereum addresses.

## What wallets are available for me to use?

Please see the [wallets](/blockchain/mainnet/wallets.html) page under our blockchain documentation. There are a number of wallet offerings, including hardware wallet support.

## How can I explore the blockchain?

You can use our explorer found at [https://explorer.permission.io](https://explorer.permission.io) to explore blocks, transactions, and addresses.

## How can I obtain ASK tokens?

Right now, you can earn ASK tokens by using the Watch-and-Earn app at [https://watch.permission.io] to watch ads.

## Where can I learn more about the Permission blockchain?

Our [whitepaper](https://permission.io/wp-content/uploads/2018/09/1536765112237_ASKWhitePaperv02.pdf) contains everything you would want to know!

## I have more questions that are not yet answered here

Feel free to drop by our [forum](https://forum.permission.io/) and our team and community can help you out!

