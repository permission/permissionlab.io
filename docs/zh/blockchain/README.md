# ASK MainNet

## MainNet Blockchain information

| | |
| --- | --- |
| Network ID | 2221 |
| Chain ID | 222 |
| Block time | 20 seconds |
| Consensus | Proof of Authority (Clique) |
| Wallet derivation path (BIP44) | ```m/44'/2221'/0'/0``` |
| Minimum required gas price | ```4,761,904,761 gASKwei``` |
| Minimum required gas limit | ```21,000 gas``` |

## Bootnodes

The list of active bootnodes can be found here:

[https://s3.amazonaws.com/ask-blockchain/mainnet/bootnodes.json](https://s3.amazonaws.com/ask-blockchain/mainnet/bootnodes.json)

```json
[
    "enode://8d21ddb79cce421495ba4c493d0b34cec964b5e007d7895c3a716d767678fc370c307ed54e24c76677721ba343cff3e50b53ffa81b46c62bac5814cb8653e65e@3.212.108.54:20202",
    "enode://acb9140deb903a6a954e6a586591a35fd710bf642e504140a4bb672e92d8c529115cb80503927450af93b37b0ac9ad32fec37a17fb5453e1960d8b00d012905b@13.58.219.101:20202",
    "enode://acdd9437aea49b0ffbfa8de046f836e4353235d56761b9c1883576448e56e70a664ff4b46094896152eacf3af76fa63d8796a5fca96667fa8961001193b83893@34.212.253.64:20202"
]
```

## Authority Nodes

The list of active authority nodes can be found here:

[https://s3.amazonaws.com/ask-blockchain/mainnet/static-nodes.json](https://s3.amazonaws.com/ask-blockchain/mainnet/static-nodes.json)

```json
[
    "enode://5da0df3ea761ffe4e61d8ce5c515815a6f5c96cd5e5cab87109829155f2eced2fa69ee561a830af4d1e893fa61e0e07fd5dab25de5db8b3da2d013a509fde690@3.212.108.54:20203",
    "enode://22c0fc7f2b38e6a030f7f908b7d34d351be9bcef6a996d92698af86359d16e2b39e35022019f3780fbf92c3a5ad3c5b80728e9a3e64436fe30f995b1175cba4c@3.212.108.54:20204",
    "enode://3eb21efb96dce3ee5791da2677904ed0a6be69cd3ad000cc57fd97e9f74272873285807834f0340c67ab0817586a7a4894fb426b6eec69799572d3f6865032c0@13.58.219.101:20203",
    "enode://79965bab3aade50e5c7d1fb63ebad8efd54965105457c98bf89f71022a7ae28fb0c1cb7f5b0eed93b0c2cb6927e86ae905ee4f0aa116fe23a49ead8fdf641bb3@13.58.219.101:20204",
    "enode://c0e05fd5c68a23749e950505c3a0deeaf5d72f29ba95ef25b30af1b6a45c25c6751fe0788d9d1cc65107953d240f6adaf05ca4aeead1c53309b1571403d5e172@34.212.253.64:20203",
    "enode://173037aa85651b19408761df1abf2026bf64101252b1d68a677f72fa649029310c7daadf133559ee8d08883c93bb95553511473a773998ea5d6473fd6ac67062@34.212.253.64:20204"
]
```

## Genesis block

The genesis block can be found here:

[https://s3.amazonaws.com/ask-blockchain/mainnet/genesis.json](https://s3.amazonaws.com/ask-blockchain/mainnet/genesis.json)

```json
{
  "config": {
    "chainId": 222,
    "homesteadBlock": 0,
    "eip150Block": 0,
    "eip155Block": 0,
    "eip158Block": 0,
    "eip160Block": 0,
    "byzantiumBlock": 0,
    "clique": {
      "period": 20,
      "epoch": 30000
    },
    "eip150Hash": "0x82f9c3255d70d01de52bc52a01ee3ba1d3b188f030c65a3d374248ee55aba07b"
  },
  "parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "nonce": "0x0",
  "timestamp": "0x5cd1f462",
  "extraData": "0x00000000000000000000000000000000000000000000000000000000000000006b4d41f0d7aabf670c8689b47e894804c8a0c835bf55e12b97d6a7f605c9c4363b07318552b3438b79793ebe4a69a9cfa9a008565a062e55c60ddae9e33aa501e18b5f6164a857d77b5ac26d631d52b6f99da04bcfc9cd738a64009d2858cf550d5ba33279d70b1b6ed67ac1e42bad4e08c5b5b20d4faf2a0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
  "gasLimit": "4000000000",
  "difficulty": "0x1",
  "mixhash": "0x0000000000000000000000000000000000000000000000000000000000000000",
  "coinbase": "0x0000000000000000000000000000000000000000",
  "alloc": {
    "0000000000000000000000000000000000000000": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000001": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000002": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000003": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000004": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000005": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000006": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000007": {
      "balance": "0x1"
    },
    "0000000000000000000000000000000000000008": {
      "balance": "0x1"
    },
    "8d3acc04be4fc16197a5fbca4717e3f6300a78bd": {
      "balance": "100000000000000000000000000000"
    }
  }
}
```
