# Running Geth

## Requirements

* Geth ([install guide](https://github.com/ethereum/go-ethereum/wiki/Building-Ethereum))
* jq (`brew install jq`, `apt-get install -y jq`, `dnf install -y jq`)

### Geth version

The ASK blockchain currently runs go-ethereum [`v1.8.19`](https://github.com/ethereum/go-ethereum/releases/tag/v1.8.19)

* Linux amd64 binary: [https://gethstore.blob.core.windows.net/builds/geth-alltools-linux-amd64-1.8.19-dae82f09.tar.gz](https://gethstore.blob.core.windows.net/builds/geth-alltools-linux-amd64-1.8.19-dae82f09.tar.gz)
* Source code (tarball): [https://github.com/ethereum/go-ethereum/archive/v1.8.19.tar.gz](https://github.com/ethereum/go-ethereum/archive/v1.8.19.tar.gz)
* Source code (tag v1.8.19): [https://github.com/ethereum/go-ethereum/tree/v1.8.19](https://github.com/ethereum/go-ethereum/tree/v1.8.19)

## Running

To run geth locally, simply copy this script and run it. It will pull in the publicly hosted mainnet genesis block and list of bootnodes.

```bash
curl -LO https://s3.amazonaws.com/ask-blockchain/mainnet/genesis.json
curl -LO https://s3.amazonaws.com/ask-blockchain/mainnet/bootnodes.json
curl -LO https://s3.amazonaws.com/ask-blockchain/mainnet/static-nodes.json
 
mkdir data
mv static-nodes.json data

geth init --datadir=./data genesis.json

```

### Running a node with fast sync

```
geth \
    --datadir ./data \
    --syncmode fast \
    --networkid 2221 \
    --rpc \
    --rpcaddr '0.0.0.0' \
    --rpcport 8545 \
    --rpcapi 'personal,db,eth,net,web3,txpool,miner,admin,debug,clique' \
    --rpccorsdomain '*' \
    --ws \
    --wsaddr '0.0.0.0' \
    --wsport 8546 \
    --wsapi 'personal,db,eth,net,web3,txpool,miner,admin,debug,clique' \
    --wsorigins '*' \
    --port 1111
    --bootnodes $(cat bootnodes.json | jq -r '. | join(",")')
```

### Running a "full" node

```
geth \
    --datadir ./data \
    --syncmode full \
    --networkid 2221 \
    --rpc \
    --rpcaddr '0.0.0.0' \
    --rpcport 8545 \
    --rpcapi 'personal,db,eth,net,web3,txpool,miner,admin,debug,clique' \
    --rpccorsdomain '*' \
    --ws \
    --wsaddr '0.0.0.0' \
    --wsport 8546 \
    --wsapi 'personal,db,eth,net,web3,txpool,miner,admin,debug,clique' \
    --wsorigins '*' \
    --port 1111
    --bootnodes $(cat bootnodes.json | jq -r '. | join(",")')
```

### Running an archive node

```
geth \
    --datadir ./data \
    --syncmode full \
    --gcmode archive \
    --networkid 2221 \
    --rpc \
    --rpcaddr '0.0.0.0' \
    --rpcport 8545 \
    --rpcapi 'personal,db,eth,net,web3,txpool,miner,admin,debug,clique' \
    --rpccorsdomain '*' \
    --ws \
    --wsaddr '0.0.0.0' \
    --wsport 8546 \
    --wsapi 'personal,db,eth,net,web3,txpool,miner,admin,debug,clique' \
    --wsorigins '*' \
    --port 1111
    --bootnodes $(cat bootnodes.json | jq -r '. | join(",")')
```


## Attaching

To attach to your Geth instance, you can use the RPC url or the ipc file descriptor that Geth creates:

### RPC Url

```bash
geth attach http://localhost:8545 
```

### IPC

```bash
geth attach data/geth.ipc
```